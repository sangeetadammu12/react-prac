import React, { Component } from 'react'
import LifecycleB from '../components/LifecylceB'

 class LifecycleA extends Component {
  constructor(props) {
    super(props)
  
    this.state = {
       name: 'sangeeta'
    }
    console.log('lifecylceA constructor')
  }

  static getDerivedStateFromProps(props) {
      console.log('lifecylceA getDerivedStateFromProps')
      return null

  }

  componentDidMount() {
      console.log('lifecylceA componentDidMount')
  }
  render() {
      console.log('lifecylceA render')
    return (
        <div>
        <div>LifecycleA </div>
        <LifecycleB/>
        </div>
      )
  }
   
  }


export default LifecycleA