import React from 'react'
import Person from './Person'

function NameList() {
    //const names =['Bruce', 'Diana', 'Lisa']
    const persons = [
        {
        id: 1,name: 'Diana',age:20, skill: "React"
        },
        {
        id: 2,name: 'Lisa',age:25, skill: "Angular"
        },
        { 
        id: 3,name: 'Clark',age:28, skill: "Vue"
        }
      ]
    //const nameList = names.map(name =><h2>{name}</h2>)
    const personList = persons.map(person =><Person key={person.id} person={person}/>)
  return (
     <div>
        {personList}
     </div>
  )
}

export default NameList