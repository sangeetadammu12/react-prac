import React from 'react'

const heading= {
    fontSize : '73px',
    color: 'blue'
}

function Inlinestyle() {
  return (
    <div>
        <h1 style={heading} >Inline</h1>
    </div>
  )
}

export default Inlinestyle