import React from 'react'


const Hello = () =>{
    // return(
    //    <div>
    //        <h1>
    //            Hello, Hello.js
    //        </h1>
    //    </div>
    // )

    //without jsx
   return React.createElement('div', {id:'hello', className: "dummyClass"}, 
   React.createElement('h1',null ,'Hello Hello.js'))

}

export default Hello  