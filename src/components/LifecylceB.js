import React, { Component } from 'react'

 class LifecycleB extends Component {
  constructor(props) {
    super(props)
  
    this.state = {
       name: 'sangeeta'
    }
    console.log('lifecylceB constructor')
  }

  static getDerivedStateFromProps(props) {
      console.log('lifecylceB getDerivedStateFromProps')
      return null

  }

  componentDidMount() {
      console.log('lifecylceB componentDidMount')
  }
  render() {
      console.log('lifecylceB render')
    return (
        <div>LifecycleB </div>
      )
  }
   
  }


export default LifecycleB