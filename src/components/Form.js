import React, { Component } from 'react'

export class Form extends Component {
    constructor(props) {
      super(props)
    
      this.state = {
         username:'',
         comments:'',
         skill:''
      }
    }

    handleChangeName =event=>{
        this.setState({username:event.target.value})
    }
    handleChangeComments =event=>{
        this.setState({comments:event.target.value})
    }
    handleChangeSkill =event=>{
        this.setState({skill:event.target.value})
    }
    submitForm = event=>{
        alert(`${this.state.username}, ${this.state.comments}, ${this.state.skill}`)
        event.preventDefault()

    }
  render() {
    return (
      <div>
          <form onSubmit={this.submitForm}>
              <div>
                  <label>Username</label>
                  <input type="text" name="username"  value={this.state.username} onChange={this.handleChangeName}/>
              </div>
              <div>
                  <label>Comments</label>
                  <input type="text" name="comments" value={this.state.comments} onChange={this.handleChangeComments}/>
              </div>
              <div>
                  <label>Select Skill</label>
                  <select name="skill" value={this.state.skill} onChange={this.handleChangeSkill} >
                      <option value="Angular">Angular</option>
                      <option value="Vue">Vue</option>
                      <option value="React">React</option>
                  </select>
              </div>
              <div>
                  <button type="submit" name="submit">Submit</button>
              </div>
          </form>
      </div>
    )
  }
}

export default Form