import React from 'react'

function Column() {
  const  items =[{id:1, title:'A'},{id:2, title:'B'},{id:3, title:'C'},{id:4, title:'D'},{id:5, title:'E'}]
  return (
    <>
        {
        items.map(item => (
           <React.Fragment key={item.id}>
             <h2>Title</h2>
             <p>{item.title}</p>
           </React.Fragment>
        ))
        }
    </>
  )
}

export default Column