import React, { Component } from 'react'

export class RefDemo extends Component {
    constructor(props) {
      super(props)
      this.inputRef = React.createRef()
      this.cbRef = null;
      this.setcbRef = ele =>{
          this.cbRef = ele
      }
    }

 componentDidMount(){
    // this.inputRef.current.focus()
    if(this.cbRef){
        this.cbRef.focus()
    }
 }
 clickHandler= () => {
    
     alert(this.inputRef.current.value)
 }
  render() {
    return (
      <div>
          <input type="text" ref={this.inputRef} />
          <input type="text" ref={this.cbRef} />
          <button onClick={this.clickHandler}>click me!</button>
      </div>
    )
  }
}

export default RefDemo