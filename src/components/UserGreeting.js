import React, { Component } from "react";

export class UserGreeting extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isLogged: false,
    };
  }
  render() {

    //if else approach
    // if(this.state.isLogged){
    //    return (<div>Welcome, Sangeeta</div>)
    // }else{
    //    return (<div>Welcome, Guest</div>)
    // }


    //element variable approach
    // let message;
    // if (this.state.isLogged) {
    //   message = <div>Welcome, Sangeeta</div>;
    // } else {
    //   message = <div>Welcome, Guest</div>;
    // }
    // return <div>{message}</div>;


    //terenary operator approach
    // return (
    // this.state.isLogged ? <div>Welcome, Sangeeta</div> : <div>Welcome, Guest</div>
    // )

    return (
    this.state.isLogged && <div>Welcome, Sangeeta</div> 
    )
  }
}

export default UserGreeting;
