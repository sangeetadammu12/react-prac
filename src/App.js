import logo from './logo.svg';
import './App.css';
import Greet from './components/Greet';
import Welcome from './components/Welcome';
import Hello from './components/Hello';
import Message from './components/Message';
import Counter from './components/Counter';
import FunctionClick from './components/FunctionClick';
import ClassClick from './components/ClassClick';
import EventBind from './components/EventBind';
import ParentComp from './components/ParentComp';
import UserGreeting from './components/UserGreeting';
import NameList from './components/NameList';
import Stylesheet from './components/Stylesheet';
import Inlinestyle from './components/Inlinestyle';
import './appStyles.css';
import ko from './appStyles.module.css'
import Form from './components/Form';
import LifecycleA from './components/LifecycleA';
import RefDemo from './components/RefDemo';

function App() {
  return (
    <div className="App">
      {/* <Form/> */}
      {/* <LifecycleA/> */}
      {/* <Table/> */}
      <RefDemo/>
      {/* <FunctionClick/>
      <ClassClick/> */}
      {/* <EventBind/> */}
      {/* <ParentComp/> */}
      {/* <UserGreeting/> */}
      {/* <NameList/> */}
      {/* <Stylesheet primary={false}/>
      <Inlinestyle/> */}
      {/* <h1 className="error">deeeeeeeeeeeee</h1>
      <h1 className={ko.success}>ceeeeeeeeeee</h1> */}
      {/* <Counter></Counter> */}
      {/* <Greet name="Sangeeta" surname="Dammu">
      </Greet> */}
      {/* <Welcome name="Bruce" heroname="Super Man"/> */}
      {/* <Hello/> */}
      {/* <Message/> */}

    </div>
  );
}

export default App;
